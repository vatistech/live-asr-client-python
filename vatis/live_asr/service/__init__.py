from vatis.live_asr.service.auth import get_auth_token

__all__ = (
    "get_auth_token"
)
