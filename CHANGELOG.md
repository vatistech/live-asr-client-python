## 1.2 
* Configuration for custom model on StreamConfig
* Templated transcription output for FormattedLiveStreamObserver
* Show only final packages flag in FormattedLiveStreamObserver

## 1.1
* Changed property names: `VATIS_LIVE_ASR_CLIENT_...` to `VATIS_ASR_CLIENT...`
* Obtain ASR service host dynamically  
* Compatibilities:
    * backend: v2.3+
    * live ASR service: v1.0+ 